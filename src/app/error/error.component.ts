import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {NgxUiLoaderService} from 'ngx-ui-loader';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {LoginValidateService} from '../core/http-api/Login/login-validate.service';
import * as _ from 'underscore';
@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit, AfterViewInit {
  useranme = 'teqodev';
  password = 'dev@123';
  errorMessage = '';

  constructor(private toastr: ToastrService,
              private loader: NgxUiLoaderService,
              private route: ActivatedRoute,
              private router: Router, private http: LoginValidateService) {
    let result = _.groupBy(['one', 'two', 'three'], 'length');
    console.log('Result----', result);
  }

  ngOnInit() {
    if (Object.keys(this.route.snapshot.queryParams).length > 0) {
      this.loader.startLoader('validate-loader');
      const params = this.route.snapshot.queryParams;
      if (Object.keys(params).length > 0) {
        // this.loader.stopLoader('validate-loader');
        if (params.username && params.password) {
          console.log('username', atob(params.username));
          this.http.validateUser(atob(params.username),params.password)
            .subscribe(
              (res: any) => {
                this.loader.stopLoader('validate-loader');
                console.log('Status----', res);
                if (res.status === 'success') {
                  setTimeout(() => {
                    this.toastr.success('successful..!!', 'Authentication');
                  }, 100);
                  setTimeout(() => {
                    localStorage.setItem('reportingUser', params.username);
                    this.router.navigate(['/report-details']);
                  }, 500);
                } else {
                  console.log('Else part');
                  this.errorMessage = 'Invalid username or password';
                  this.toastr.error('Authentication failed.Please provide correct details', 'Authentication');
                  this.clearLocalStorage();
                }
              },
              (error) => {
                console.log('Error in validating');
                this.loader.stopLoader('validate-loader');
                this.errorMessage = 'Internal server error';
              }
            );
        } else if (params.username) {
          setTimeout(() => {
            this.errorMessage = 'Please provide password';
            this.toastr.error('Please provide password', 'Error');
            this.loader.stopLoader('validate-loader');
            this.clearLocalStorage();
          }, 500);

        } else {
          setTimeout(() => {
            this.errorMessage = 'Please provide username';
            this.toastr.error('Please provide username', 'Error');
            this.loader.stopLoader('validate-loader');
            this.clearLocalStorage();
          }, 500);

        }
      } else {
        this.loader.stopLoader('validate-loader');
        this.errorMessage = 'Please provide username and password...!!';
        setTimeout(() => {
          this.toastr.error('Please provide username and password in query params.', 'Error');
        }, 500);
        this.clearLocalStorage();
      }
    } else {
      this.errorMessage = 'Please provide username and password..!';
      this.clearLocalStorage();
    }
  }

  ngAfterViewInit(): void {
    /*this.loader.startLoader('validate-loader');
    this.route.queryParams.subscribe(
      (params) => {
        if (Object.keys(params).length > 0) {
          this.loader.stopLoader('validate-loader');
          if (params.username && params.password) {
            console.log('username', atob(params.username));
            if (atob(params.username) === this.useranme && this.password === atob(params.password)) {
              this.router.navigate(['/report-details'], {queryParamsHandling: 'preserve'});
            } else {
              this.errorMessage = 'Invalid username or password';
              this.toastr.error('Authentication failed.Please provide correct details', 'Authentication Error');
            }
          } else if (params.username) {
            this.errorMessage = 'Please provide password';
            this.toastr.error('Please provide password', 'Error');
          } else {
            this.errorMessage = 'Please provide username';
            this.toastr.error('Please provide username', 'Error');
          }
        } else {
          this.loader.stopLoader('validate-loader');
          this.errorMessage = 'Please provide username and password...!!';
          setTimeout(() => {
            this.toastr.error('Please provide username and password in query params.', 'Error');
          }, 500);
        }
      }
    );*/
  }

  clearLocalStorage() {
    localStorage.removeItem('site');
    localStorage.removeItem('reportingUser');
  }
}
