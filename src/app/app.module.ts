import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxUiLoaderModule} from 'ngx-ui-loader';
import { ErrorComponent } from './error/error.component';
import {ActivatedRouteSnapshot, RouterModule} from '@angular/router';
import {AuthGuard} from './core/guards/auth-guard';
import {LoginValidateService} from './core/http-api/Login/login-validate.service';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxUiLoaderModule
  ],
  providers: [LoginValidateService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
