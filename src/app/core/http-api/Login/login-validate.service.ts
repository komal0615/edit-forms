import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../../environments/environment";

@Injectable()
export class LoginValidateService {
  constructor(private http: HttpClient) {
  }

  validateUser(username, password) {
    return this.http.post(environment.baseURL+ '/validate-user', {username: btoa(username), password: password});
  }
}
