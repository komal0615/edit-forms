import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ReportDownloadModel} from '../../../shared/validate-models/validate-model';
import {environment} from "../../../../environments/environment";


@Injectable()
export class ReportingHttpService {
  constructor(private http: HttpClient) {
  }

  getUserSpecificSites(username: string) {
    return this.http.post(environment.baseURL + '/get-sites', {username: username});
  }

  getAllReports(sitename: string) {
    return this.http.post(environment.baseURL + '/get-reports', {site: sitename});
  }

  getReportsProgress(username: string) {
    return this.http.post(environment.baseURL + '/get-progress', {userName: username});
  }

  getSiteSpecificDevices(sitename: string) {
    return this.http.post(environment.baseURL + '/get-devices-list', {site: sitename});
  }

  getSiteSpecificParams(sitename: string) {
    return this.http.post(environment.baseURL + '/get-device-params/', {site: sitename});
  }

  saveReportConfig(config: ReportDownloadModel) {
    return this.http.post(environment.baseURL + '/save-report/', config);
  }

  getResolution(siteName) {
    return this.http.post(environment.baseURL + '/getSiteResolution/', {site: siteName});
  }

  getResolutionParams(siteName, resolution) {
    return this.http.post(environment.baseURL + '/getResolutionDeviceParams/', {"site" : siteName,
      "resolutionType" : resolution});
  }
  getSiteSpecificReportsStatus(sitename) {
    return this.http.post(environment.baseURL + '/get-progress/', {site: sitename});
  }

  deleteSiteSpecificReport(reportid, sitename) {
    return this.http.post(environment.baseURL + '/delete-report/', {reportId: reportid, site: sitename});
  }

  cancelReportProcessing(reportid, sitename) {
    return this.http.post(environment.baseURL + '/cancel-report/', {reportId: reportid, site: sitename});
  }
}

