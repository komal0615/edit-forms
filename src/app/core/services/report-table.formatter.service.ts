import {Injectable} from '@angular/core';
import {ReportDownloadModel} from '../../shared/validate-models/validate-model';
import * as moment from 'moment';
@Injectable()
export class ReportTableFormatterService {
  constructor() {
  }

  getTableFormattedService(tableData) {
    let result = [];
    tableData.forEach((item: ReportDownloadModel) => {
      result.push({
        id: item._id.reportId,
        site: item._id.site,
        reportName: item.reportName,
        startDate: new Date(item.dateRange.startDate),
        endDate: new Date(item.dateRange.endDate),
        dateRange: {
          start: moment(item.dateRange.startDate).format('DD/MM/YYYY'),
          end: moment(item.dateRange.endDate).format('DD/MM/YYYY'),
        },
        schedule: 'on-demand',
        status: item.status,
        progress: item.progress,
        downloadedPath: item.downloadedPath,
        //downloadedPath: 'http://13.233.69.83:40141/reports/download?fileName=D:\\erixis\\solarpulse\\binaries\\RTA.jar',
        fileSize: item['FileSize_MB'],
        deviceId: item.deviceIds,
        email: item.email,
        format: item.format,
        parameters: item.parameters,
        resolutionType: item.reportType,
        deviceType: item.deviceType,


      });
    });
    return result;
  }
}
