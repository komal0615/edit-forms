import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewReportComponent} from './components/new-report/new-report.component';
import {ReportDetailsComponent} from './components/report-details/report-details.component';
import {BsDatepickerModule, ModalModule, ProgressbarModule} from 'ngx-bootstrap';
import {SelectDropDownModule} from 'ngx-select-dropdown';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule, MatPaginatorModule, MatSortModule} from '@angular/material';
import {ModalDirectivesDirective} from './directives/modal-directives.directive';
import {ReportingHttpService} from '../../core/http-api/report-module-api/reporting-http.service';
import {ReportTableFormatterService} from '../../core/services/report-table.formatter.service';
import {ToastrModule} from 'ngx-toastr';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {NgxUiLoaderModule} from 'ngx-ui-loader';
import {RouterModule} from '@angular/router';
import {ButtonModule, InputTextModule, ProgressBarModule, TooltipModule} from "primeng/primeng";
import {TableModule} from "primeng/table";
import { EditFormsComponent } from './components/edit-forms/edit-forms.component';

@NgModule({
  declarations: [NewReportComponent, ReportDetailsComponent, ModalDirectivesDirective, EditFormsComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgxDaterangepickerMd.forRoot(),
        ModalModule.forRoot(),
        ProgressbarModule.forRoot(),
        SelectDropDownModule,
        MatIconModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        ToastrModule.forRoot(),
        AngularFontAwesomeModule,
        NgxUiLoaderModule,
        ProgressBarModule,
        TooltipModule,
        TableModule,
        RouterModule,
        InputTextModule,
        ButtonModule,
        BsDatepickerModule.forRoot()
    ],
  providers: [ReportingHttpService, ReportTableFormatterService],
  exports: [NewReportComponent, ReportDetailsComponent]
})
export class ReportingModule {
}
