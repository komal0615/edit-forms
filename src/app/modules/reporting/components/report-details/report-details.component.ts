import {AfterViewChecked, AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ReportingHttpService} from '../../../../core/http-api/report-module-api/reporting-http.service';
import {ReportTableFormatterService} from '../../../../core/services/report-table.formatter.service';
import {ToastrService} from 'ngx-toastr';
import {ReportDownloadModel} from '../../../../shared/validate-models/validate-model';
import {NgxUiLoaderModule, NgxUiLoaderService} from 'ngx-ui-loader';
import {SiteGetSitesViewItem} from '../../../../shared/Models/get-sites-view-model';
import {HttpClient} from '@angular/common/http';
import {Subscription, timer} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';

export interface ReportDetails {
  reportName: string;
  dateRange: { start: string, end: string };
  schedule: string;
  progress: number;
}

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.scss']
})
export class ReportDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('downloadpop') downloadPopup;
  @ViewChild('deletepopup') deletePopup;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'reportName', 'dateRange', 'status', 'progress', 'actions'];
  dataSource = new MatTableDataSource();
  sitesData = {sites: []};
  tableData: any[] = [];
  loaderMessage = '';
  defaultSite = '';
  progressInterval: any;
  reportNameToDownload: string;
  isDeleteConfirm = false;
  recordToDelete;
  subScription: Subscription;
  isDataLoaded = true;
  isServerError = false;
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router,
              public http: ReportingHttpService,
              private formatter: ReportTableFormatterService,
              private toastr: ToastrService,
              private loader: NgxUiLoaderService,
              private route: ActivatedRoute, private httpNative: HttpClient) {


  }

  ngOnInit() {
    const username = localStorage.getItem('reportingUser');
    this.getSites(atob(username));
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    this.subScription.unsubscribe();
  }


  onNewReport() {
    clearInterval(this.progressInterval);
    this.router.navigate(['/new-report'], {queryParamsHandling: 'preserve'});
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getSites(username) {
    this.loaderMessage = 'Loading sites...';
    this.loader.startLoader('site-loader');
    this.http.getUserSpecificSites(username).subscribe(
      (success: SiteGetSitesViewItem[]) => {
        this.loader.stopLoader('site-loader');
        this.sitesData = {sites: success};
        if (localStorage.getItem('site')) {
          console.log('If part');
          this.defaultSite = localStorage.getItem('site');
          this.getAllReports(localStorage.getItem('site'));

        } else {
          console.log('else part');
          localStorage.setItem('site', this.sitesData.sites [0].site);
          this.getAllReports(localStorage.getItem('site'));
          this.defaultSite = localStorage.getItem('site');
        }
      },
      (error) => {
        this.loader.stopLoader('site-loader');
        this.toastr.error('Failed to load sites. Please try after some time...! ', 'Status');
      }
    );
  }

  getAllReports(sitename) {
    this.loader.startLoader('tableData');
    this.subScription = timer(0, environment.tableRefreshTime).pipe(
      switchMap(() => {
        return this.http.getAllReports(localStorage.getItem('site'));
      })
    ).subscribe(
      (reportsData: []) => {
        this.loader.stopLoader('tableData');
        this.tableData = [...reportsData];
        if (this.tableData.length > 0) {
          this.isDataLoaded = true;
          this.tableData = this.formatter.getTableFormattedService(reportsData.reverse());
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        } else {
          this.isDataLoaded = false;
          this.dataSource.data = [];
        }
      },
      (error) => {
        this.isServerError = true;
        this.isDataLoaded = true;
        this.loader.stopLoader('tableData');
      }
    );
    /*this.http.getAllReports(sitename).subscribe(
      (reportsData: []) => {
        this.loader.stopLoader('tableData');
        this.tableData = [...reportsData];
        if (this.tableData.length > 0) {
          this.dataSource.data = this.formatter.getTableFormattedService(reportsData.reverse());

          setTimeout(() => {
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          }, 1000)

          //clearInterval(this.progressInterval);
          //this.progressInterval = setInterval(this.triggerProgressAPI.bind(this, this.tableData.reverse()), 1200);
        } else {
          clearInterval(this.progressInterval);
          // this.triggerProgressAPI(this.tableData);
        }
      },
      (error) => {
        this.loader.stopLoader('tableData');
      }
    );*/
  }

  triggerProgressAPI(data) {
    this.http.getSiteSpecificReportsStatus(localStorage.getItem('site'))
      .subscribe((reportStatus: any[]) => {
          data.forEach((report: ReportDownloadModel, index) => {
            reportStatus.forEach((record) => {
              if (report._id.reportId === record._id.reportId) {
                report.progress = record.progress;
                report.status = record.status;
                report.downloadedPath = record.downloadedPath;
              }
            });

          });
          this.dataSource.data = this.formatter.getTableFormattedService(data);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },
        (error) => {
          console.log('Error in get progress api', error);
        }
      );
  }

  changeSite(event) {
    this.defaultSite = event.target.value;
    localStorage.setItem('site', event.target.value);
    this.getAllReports(event.target.value);
  }

  onDownloadModal(reportInfo) {
    console.log('reportInfo----', reportInfo);
    if (reportInfo.status === 'Completed') {
      console.log('If Part');
      this.downloadPopup.show();
      this.reportNameToDownload = reportInfo.downloadedPath;
    }
    /*else{
      console.log('else Part');
      if(reportInfo.status === 'Completed'){
        this.http.downloadReports(reportInfo.downloadedPath).subscribe(
          (data) => {
            var blob = new Blob([data]);
            saveAs(blob,`${reportInfo.id}.zip`);
          },
          (error) => {
            alert("Error while downloading...!!")
          }
        )
      }
    }*/
  }

  isDownloadDisabled(reportInfo) {
    if (reportInfo.status === 'Completed') {
      return true;
    } else {
      return false;
    }
  }

  onDeleteReport(element: any) {
    if (element.status === 'Completed' || element.status === 'Cancel') {
      this.deletePopup.show();
      this.recordToDelete = element;
    }
  }

  confirm() {
    this.deletePopup.hide();
    if (this.recordToDelete.status === 'Completed' || this.recordToDelete.status === 'Cancel') {
      this.http.deleteSiteSpecificReport(this.recordToDelete.id, this.recordToDelete.site)
        .subscribe((data) => {
            this.getAllReports(this.recordToDelete.site);
          },
          (error) => {
            console.log('Error while deleting--', error);
          }
        );
    }
  }

  decline() {
    this.deletePopup.hide();
  }

  cancelReportProcessing(element) {
    console.log('Its called--');
    if (element.status === 'Completed' || element.status === 'Zipping' || element.status === 'Cancel') {
      alert('This action cannot be process..!!');
    } else {
      this.http.cancelReportProcessing(element.id, element.site).subscribe(
        (data) => {
          console.log('Report Successfully canceled...!!');
        }
      );
    }

  }

  onReportEdit(report: any) {

   localStorage.setItem('reportDetails', JSON.stringify(report));
    const retrievedObject = localStorage.getItem('reportDetails');

    console.log('retrievedObject: ', JSON.parse(retrievedObject));
      this.router.navigate(['/edit-form']);
  }
}
