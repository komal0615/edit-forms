import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {DaterangepickerDirective} from 'ngx-daterangepicker-material';
import {GetSitesViewModel, SiteGetSitesViewItem} from '../../../../shared/Models/get-sites-view-model';
import {ReportModel} from '../../../../shared/validate-models/report-model';
import {GetEmailsViewModel} from '../../../../shared/Models/get-emails-view-model';
import {configParametersViewModel, GetParameterViewItem} from '../../../../shared/Models/get-params-view-model';
import {ParametersViewModel, ParameterViewItem, SelectorViewItem} from '../../../../shared/Models/params-view-model';
import {DeviceViewItem, ZonesViewModel, ZoneViewItem} from '../../../../shared/Models/zones-view-model';
import {GetTypesViewModel, GetZoneViewItem} from '../../../../shared/Models/get-types-view-model';
import * as moment from 'moment';
import {ReportDownloadModel, ValidateModel} from '../../../../shared/validate-models/validate-model';
import {NotificationModel} from '../../../../shared/validate-models/notification-model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ReportingHttpService} from '../../../../core/http-api/report-module-api/reporting-http.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxUiLoaderService} from 'ngx-ui-loader';
import {APP_CONST} from '../../../../shared/constant/app-config';

const DEVIDE_COUNT = {
  'Minute': 1440,
  'Daily': 1,
  '15_min': 97,
  '5_min': 289,
  '10_min': 145,
  '30_min': 49,
  '45_min': 33,
  '60_min': 25,
  'Generation_Hour': 1,
  'Non_Generation_Hour': 1
}
const resolution_mapper = {
  '5_min': '5_Minute',
  '10_min': '10_min',
  '15_Minute': '15_min',
  '30_Minute': '30_Minute',
  '45_Minute': '45_min',
  '60_Minute': '60_min',
}

const reverse_resolution_mapper = {
  '5_min': '5_Minute',
  '10_min': '10_Minute',
  '15_min': '15_Minute',
  '30_min': '30_Minute',
  '45_min': '45_Minute',
  '60_min': '60_Minute',
}

@Component({
  selector: 'app-edit-forms',
  templateUrl: './edit-forms.component.html',
  styleUrls: ['./edit-forms.component.scss']
})
export class EditFormsComponent implements OnInit {
  editObject = JSON.parse(localStorage.getItem('reportDetails'));
  repName = '';
  dateFormat = 'DD/MM/YYYY'
  RESOLUTIONS_TYPE = [];
  typesDictionary = [
    {
      type: 'INV',
      aliases: [
        'INV',
        'INVERTER'
      ]
    },
    {
      type: 'INVERTER',
      aliases: [
        'INVERTER',
        'INV'
      ]
    }
  ];
  REPORT_FORMAT = ['csv'];
  newSelectedResolution = '';
  newSelectedDeviceType = '';
  displayDate = '';
  isDeviceTypeSelected = false;
  displayModalTitle = '';
  reportDownLoadModel: ReportDownloadModel;
  deviceCount = 0;
  parametersCount = 0;
  selectedDeviceCount = 0;
  selectedParamsCount = 0;
  totolDays = 0;
  loaderMessage = '';
  dataPointCount = 0;
  totalCapping = APP_CONST.cappingConfig.cappingPoints;
  messageInterval = 0;
  emailsToDisplay = [];
  isEmailLimitExceeds = false;
  @ViewChild('openDeviceBoxElement') openDeviceBoxElement: ElementRef;
  @ViewChild('openParamsBoxElement') openParamsBoxElement: ElementRef;
  @ViewChild('paramsSearchInput') paramsSearchInput: ElementRef;
  @ViewChild('zonesSearchInput') zonesSearchInput: ElementRef;
  @ViewChild(DaterangepickerDirective) pickerDirective: DaterangepickerDirective;
  @ViewChild('paramsModal') paramsModal;
  @ViewChild('deviceModal') deviceModal;
  @ViewChild('selectedDeviceModal') selectedDeviceModal;
  @ViewChild('selectedParamsModal') selectedParamsModal;
  // Viev validate-models
  public sitesData: GetSitesViewModel;
  public model: ReportModel;
  public emailsData: GetEmailsViewModel;
  public parametersViewModel: configParametersViewModel;
  public paramsModel: ParametersViewModel;
  public paramsBufferModel: ParametersViewModel;
  public zonesViewModel: ZonesViewModel;
  public zonesBufferModel: ZonesViewModel;
  public deviceModel: GetTypesViewModel;
  // Arrays
  public shownDevicesList: string[];
  public deviceTypes: string[];
  public paramsList: GetParameterViewItem[];
  public shownParamsList: string[];
  public tempValues: string[];
  public devices: string[];


  // Date info
  public ranges: any = {
    'Todays': [moment(), moment()],
    'Last Week': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
    'Last 15 Days': [moment().subtract(15, 'days'), moment().subtract(1, 'days')],
    'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
    'Last 90 Days': [moment().subtract(90, 'days'), moment().subtract(1, 'days')]
  };
  public range = {start: moment(), end: moment()};
  public maxDate: moment.Moment;
  public minDate: moment.Moment;

  // DropDown
  public openDeviceBoxModal: boolean;
  public openParamsBoxModal: boolean;
  public validateModel: ValidateModel;
  public notifications: NotificationModel;
  public temporatyEmail: string;

  // Forms
  public reportForm: FormGroup;

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private http: ReportingHttpService, private router: Router, private toastr: ToastrService, private loader: NgxUiLoaderService) {
    this.sitesData = new GetSitesViewModel();
    this.model = new ReportModel();
    this.emailsData = new GetEmailsViewModel();
    this.parametersViewModel = new configParametersViewModel();
    this.paramsModel = new ParametersViewModel();
    this.paramsBufferModel = new ParametersViewModel();
    this.zonesViewModel = new ZonesViewModel();
    this.zonesBufferModel = new ZonesViewModel();
    this.deviceModel = new GetTypesViewModel();
    this.validateModel = new ValidateModel();
    this.notifications = new NotificationModel();
    this.displayDate = moment(this.range.start).format('DD/MM/YYYY') + ' - ' + moment(this.range.end).format('DD/MM/YYYY');
    this.reportDownLoadModel = new ReportDownloadModel();
    this.reportForm = new FormGroup({
      reportName: new FormControl([Validators.required]),
      siteName: new FormControl(null, [Validators.required]),
      resolution: new FormControl(null, [Validators.required]),
      format: new FormControl(null, [Validators.required]),
      dateRange: new FormControl(),
      deviceType: new FormControl(null, [Validators.required]),
      emails: new FormControl(null, [Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      isSingleFile: new FormControl(true),
      deviceList: new FormControl(null, [Validators.required]),
    });
    this.calculateTotalDays(this.range);

    this.getSites();
    this.fetchResolution(this.editObject.site);
    this.getDevices(this.editObject.site, true);
    this.fetchParams(this.editObject.site, reverse_resolution_mapper[this.editObject.resolutionType])
    //this.changeResolution(reverse_resolution_mapper[this.editObject.resolutionType]);

  }

  @HostListener('document:click', ['$event.target']) onClick() {
    this.hideModal(event);
  }

  ngOnInit() {
    let reportDetails = window.localStorage.getItem('reportDetails');
    console.log("report Details-----", reportDetails);
    if (!reportDetails) {
      alert('Invalid action.');
      this.router.navigate(['report-details']);
      return;
    }
    this.shownDevicesList = new Array<string>();
    this.shownParamsList = new Array<string>();
    this.devices = new Array<string>();
    this.paramsList = [];
    this.maxDate = moment();
    // ReportName value
    this.reportForm.get('reportName').valueChanges.subscribe(
        (reportname) => {
          if (reportname) {
            this.repName = reportname;
            this.validateModel.isReportNameEntered = true;
          } else {
            this.validateModel.isReportNameEntered = false;
          }

        }
    );
    this.reportForm.patchValue({
      reportName: this.editObject.reportName,
      format: this.editObject.format

    })
  }

  /*-------------------------Data functions-------------------------------------*/
  // return sites data
  public getSites() {
    this.loaderMessage = 'Loading sites...';
    this.loader.startLoader('api-loader');
    this.http.getUserSpecificSites(atob(localStorage.getItem('reportingUser'))).subscribe(
        (success: SiteGetSitesViewItem[]) => {
          this.loader.stopLoader('api-loader');
          this.sitesData = {sites: success};
          this.reportForm.patchValue({
            siteName: this.editObject.site
          })
        },
        (error) => {
          this.loader.stopLoader('api-loader');
          this.toastr.error('Failed to load sites. Please try after some time...! ', 'Status');
          console.log('Error while calling sites api', error);
        }
    );
  }

  // calling api for getdevice
  getDevices(sitename, isEditDevices) {
    this.loaderMessage = `Loding device for site ${sitename}`;
    this.loader.startLoader('api-loader');
    this.http.getSiteSpecificDevices(sitename).subscribe(
        (devicesData: GetTypesViewModel) => {
          this.loader.stopLoader('api-loader');
          this.deviceModel = new GetTypesViewModel();
          devicesData.typesDictionary = this.typesDictionary;
          this.deviceTypes = [];
          this.deviceModel = devicesData;
          console.log('Device Data----', devicesData);
         if(isEditDevices) {
            devicesData.types.forEach((l1) => {
              console.log('l1----', l1);
              l1.zones.forEach((l2) => {
                console.log('l2----', l2);
                l2.devices.forEach((l3) => {
                  console.log('l3----', l3);
                  this.editObject.deviceId.forEach((deviceId) => {
                    if(l3.deviceId === deviceId) {
                      l3.deviceState = true;
                    } else {
                      l3.deviceState = false;
                    }
                  });
                });
              });
            });
          }
          //this.deviceTypes = this.deviceModel.types.map(x => x.type);  // device Type array for device dropdown
          console.log('------DeviceTypes1------', this.deviceTypes);
        },
        (error) => {
          this.loader.stopLoader('api-loader');
          this.toastr.error('Failed to load devices. Please try after some time...! ', 'Status');
          console.log('Error in http-api for getsitespecific devices');
        }
    );
  }

  /*----------------------------------------------------------------------------*/

  // site dropdown
  public changeSite(event) {
    // this.reset();
    if (this.repName) {
      this.reportDownLoadModel.reportName = this.repName;
      this.validateModel.isReportNameEntered = true;
    } else {
      this.validateModel.isReportNameEntered = false;
    }
    if (this.reportForm.value.siteName) {
      // this.fetchResolution(this.reportForm.value.siteName);
      let siteVal = this.sitesData.sites.find((siteInfo) => {
        return siteInfo.site === event.target.value;
      });
      //this.formResetFromResolution();


      this.validateModel.isSiteSelected = true;
      this.model.site = this.reportForm.value.siteName;
      const siteName = this.reportForm.value.siteName;
      this.reportDownLoadModel._id.site = siteName;
      const siteItem = this.sitesData.sites.find(x => x.site === siteName);
      this.messageInterval = siteItem['messageInterval'];
      this.minDate = moment(siteItem.dateOfCommissioning);
      // this.getDevices(this.model.site);
    } else {
      this.validateModel.isSiteSelected = false;
      return;
    }
  }

  fetchResolution(siteName) {
    console.log('fetchh resolution called', siteName);
    this.http.getResolution(siteName).subscribe(
        (result) => {
          this.RESOLUTIONS_TYPE = result['reportResolutionType'];
          this.reportForm.patchValue({
            resolution: reverse_resolution_mapper[this.editObject.resolutionType]
          })
        }
    )
  }

  // resolution dropdown
  onChangeResolution() {
    this.newSelectedResolution = this.reportForm.value.resolution;
    //this.getDevices(this.reportForm.get('siteName').value);
    //this.fetchParams(this.reportForm.get('siteName').value, this.newSelectedResolution);
    this.changeResolution(this.newSelectedResolution);
  }

  fetchParams(siteName, resolution) {
    this.http.getResolutionParams(siteName, resolution).subscribe(
        (result: configParametersViewModel) => {
          this.parametersViewModel = result;
          this.deviceTypes = this.parametersViewModel.resolutionDeviceTypesList;
          console.log('------DeviceTypes2------', result);
          this.reportForm.patchValue({
            deviceType: this.editObject.deviceType
          });
          this.changeDevice(this.editObject.deviceType);
          //this.parametersViewModel.resolutionDeviceTypesList = this.intersection(this.deviceTypes, reportDeviceList);
        }
    )
  }

  public changeResolution(resolutionType: string) {
    //this.formResetFromFormat();
    if (resolutionType) {
      this.validateModel.isResolutionSelected = true;
      this.model.resolutionType = resolutionType;
      this.reportDownLoadModel.reportType = resolutionType === 'Daily' || resolutionType === 'Minute' || resolutionType === 'Generation_Hour' || resolutionType === 'Non_Generation_Hour' ? resolutionType : resolution_mapper[resolutionType];
      console.log('reportType----', this.reportDownLoadModel.reportType);
      this.setMaxDate(resolutionType);
      this.resetDeviceSelectionBlock();
      this.checkForDevices(this.deviceTypes, resolutionType);
    }
    if (!resolutionType) {
      this.validateModel.isResolutionSelected = false;
    }
  }

  // report format dropdown
  onChangeFormat(event) {
    if (this.reportForm.value.format) {
      this.validateModel.isReportFormatSelected = true;
      this.reportDownLoadModel.format = this.reportForm.value.format;
      this.model.format = this.reportForm.value.format;
    } else {
      this.validateModel.isReportFormatSelected = false;
    }
  }

  // on date range change
  public changeRange(event: any) {
    if (!event.start) {
      return;
    }
    this.range = {
      start: moment(event.start),
      end: moment(event.end)
    };
    this.reportDownLoadModel.dateRange.startDate = moment(this.range.start).startOf('day').utc().milliseconds(0).toISOString();
    this.reportDownLoadModel.dateRange.endDate = moment(this.range.end).endOf('day').utc().milliseconds(0).toISOString();
    const firstDate = this.range.start;
    const secondDate = this.range.end;
    this.displayDate = moment(this.range.start).format('DD/MM/YYYY') + ' - ' + moment(this.range.end).format('DD/MM/YYYY');
    const daysCount = secondDate.diff(firstDate, 'days');
    this.totolDays = daysCount === 0 ? 1 : daysCount;
    this.calculateCappingPoints();
    if (daysCount <= 365 && firstDate && secondDate) {
      this.validateModel.isDateRangeValid = true;
    } else {
      this.validateModel.isDateRangeValid = false;
    }
  }

  // on device type dropdown
  onChangeDeviceType() {
    this.newSelectedDeviceType = this.reportForm.value.deviceType;
    this.changeDevice(this.newSelectedDeviceType);
  }

  public changeDevice(deviceType: string) {
    if (deviceType) {
      this.isDeviceTypeSelected = true;
      this.validateModel.isDeviceTypeSelected = true;
      const typeAliasList = this.getTypeAlieses(deviceType);
      this.getZones(typeAliasList);
      this.getParameresModel(deviceType);
      this.model.typeAliasList = typeAliasList;
      this.model.deviceType = deviceType;
      this.reportDownLoadModel.deviceType = deviceType;
      this.reportDownLoadModel._id.reportId = deviceType + '_' + moment().valueOf();
      this.calculateCappingPoints();
    } else {
      this.isDeviceTypeSelected = false;
      this.validateModel.isDeviceTypeSelected = false;
      return;
    }
    this.shownDevicesList = new Array<string>();
    this.shownParamsList = new Array<string>();
  }

  public intersection(arrayA: string[], arrayB: string[]) {
    const typesSet = new Set<string>(arrayA);
    this.deviceModel.typesDictionary.forEach(typeElement => {
      const type = typeElement.type;
      if (arrayA.some(x => x === type)) {
        const aliases = typeElement.aliases;
        aliases.forEach(alias => {
          typesSet.add(alias);
        });
      }
    });
    return Array.from(typesSet.values()).filter(value => -1 !== arrayB.indexOf(value)).sort();
  }

  public getTypeAlieses(deviceType: string): string[] {
    const aliases = new Set<string>();
    aliases.add(deviceType);
    this.deviceModel.typesDictionary.forEach(typeElement => {
      if (typeElement.aliases.some(x => x === deviceType)) {
        typeElement.aliases.forEach(alias => {
          aliases.add(alias);
        });
      }
    });
    return Array.from(aliases.values());
  }

  public getZones(deviceType: string[]) {
    const typeViewItem = this.deviceModel.types.find(value => -1 !== deviceType.indexOf(value.type));
    if (typeViewItem) {
      const zoneJsonValue = JSON.stringify(typeViewItem);
      this.zonesBufferModel = JSON.parse(zoneJsonValue);
      // tslint:disable-next-line:max-line-length
      this.zonesBufferModel.zones.sort((a, b) => (a.zone.toLowerCase() > b.zone.toLowerCase()) ? 1 : ((b.zone.toLowerCase() > a.zone.toLowerCase()) ? -1 : 0));
    } else {
      this.zonesBufferModel = new ZonesViewModel();
    }
    this.deviceCount = this.zonesBufferModel.zones.length;
    this.calculateSelectedDevices([...this.zonesBufferModel.zones]);
    this.zonesViewModel = this.zonesBufferModel;
  }

  public getParameresModel(deviceType: string) {
    this.paramsModel = new ParametersViewModel();
    const selectorList = new Array<string>();
    this.paramsBufferModel = new ParametersViewModel();
    this.paramsList = this.getParameters(deviceType);
    if (!this.paramsList) {
      this.validateModel.isParamsExist = false;
      return;
    }
    for (let index = 0; index < this.paramsList.length; index++) {
      const element = this.paramsList[index];
      const selector = element.displayName.charAt(0).toUpperCase();
      const concurrence = selectorList.find(x => x === selector);
      if (!concurrence) {
        selectorList.push(selector.toUpperCase());
      }
    }
    for (let index = 0; index < selectorList.length; index++) {
      const selector = selectorList[index];
      const paramsItem = new SelectorViewItem();
      paramsItem.paramsSelector = selector;
      const filtredParams = this.paramsList.filter(x => x.displayName.charAt(0).toUpperCase() === selector);
      filtredParams.forEach(paramObj => {
        paramObj.paramState = true;
        paramsItem.parameters.push(paramObj);
      });
      this.paramsBufferModel.paramsList.push(paramsItem);
      this.paramsModel = this.paramsBufferModel;
    }
  }

  public getParameters(deviceType: string): GetParameterViewItem[] {
    const deviceObject = this.parametersViewModel.resolutionDeviceTypes.find(x => x.type === deviceType);
    if (deviceObject) {
      this.calculateSelectedParams(deviceObject.parameters);
      return deviceObject.parameters.sort((a, b) => (a.displayName.toLowerCase() > b.displayName.toLowerCase()) ? 1 : ((b.displayName.toLowerCase() > a.displayName.toLowerCase()) ? -1 : 0));
    }
    return null;
  }

  public getShownDevicesList() {
    const shownList = new Array<string>();
    let allSelected = true;
    this.zonesBufferModel.zones.forEach(zone => {
      zone.devices.forEach((device) => {
        if (device.deviceState) {
          shownList.push(device.displayName);
        } else {
          allSelected = false;
        }
      });
    });
    if (allSelected) {
      this.shownDevicesList = new Array<string>();
    }
    if (!allSelected) {
      this.shownDevicesList = shownList;
    }
  }

  public getShownParamsList() {
    const shownList = new Array<string>();
    let allSelected = true;
    this.paramsBufferModel.paramsList.forEach((parameter) => {
      parameter.parameters.forEach((param) => {
        if (param.paramState) {
          shownList.push(param.displayName);
        } else {
          allSelected = false;
        }
      });
    });
    if (allSelected) {
      this.shownParamsList = new Array<string>();
    }
    if (!allSelected) {
      this.shownParamsList = shownList;
    }
  }

  public toggleDatePicker(event: any) {
    this.pickerDirective.toggle(event);
  }

  public hideModal(event: any) {
    if (this.openParamsBoxModal && !this.openParamsBoxElement.nativeElement.contains(event.target)) {
      this.getShownParamsList();
      this.openParamsBoxModal = !this.openParamsBoxModal;
    }
    if (this.openDeviceBoxModal && !this.openDeviceBoxElement.nativeElement.contains(event.target)) {
      this.getShownDevicesList();
      this.openDeviceBoxModal = !this.openDeviceBoxModal;
    }
  }

  public applyDevices() {
    const selectedDevices = [];
    this.model.devices = [];
    this.model.deviceModels = [];
    this.reportDownLoadModel.deviceIds = [];
    this.zonesViewModel.zones.forEach((zone) => {
      zone.devices.forEach((device) => {
        if (device.deviceState) {
          selectedDevices.push(device.deviceId);
          this.reportDownLoadModel.deviceIds.push(device.deviceId);
          this.model.deviceModels.push(device);
        }
      });
    });
    this.getShownDevicesList();
    this.model.devices = selectedDevices;
    if (this.selectedDeviceCount > 0) {
      this.validateModel.isDevicesSelected = true;
    } else {
      this.validateModel.isDevicesSelected = false;
    }
    this.openDeviceBoxModal = !this.openDeviceBoxModal;
    this.deviceModal.hide();
  }

  public applyParams() {
    const selectedParams = new Array<GetParameterViewItem>();
    this.model.selectedParameters = [];
    this.reportDownLoadModel.parameters = [];
    this.paramsBufferModel.paramsList.forEach((parameter) => {
      parameter.parameters.forEach((param) => {
        if (param.paramState) {
          selectedParams.push(param);
          this.reportDownLoadModel.parameters.push(param);
        }
      });
    });
    this.getShownParamsList();
    this.model.selectedParameters = selectedParams;
    if (this.selectedParamsCount > 0) {
      this.validateModel.isParametersSelected = true;
    } else {
      this.validateModel.isParametersSelected = false;
    }
    this.paramsModal.hide();
  }

  public cancelDevices() {
    this.changeStateForAllDevices(true);
    this.shownDevicesList = [];
    this.openDeviceBoxModal = false;
    this.deviceModal.hide();
  }

  public cancelParams() {
    this.changeStateForAllParams(true);
    this.shownParamsList = [];
    this.openParamsBoxModal = false;
    this.paramsModal.hide();
  }

  public changeStateForAllDevices(flag: boolean) {
    this.zonesViewModel.zones.forEach((zone) => {
      zone.devices.forEach((device) => {
        this.changeDeviceState(zone.devices, device.deviceId, flag);
      });
    });
  }

  public changeStateForAllParams(flag: boolean) {
    this.paramsModel.paramsList.forEach((element) => {
      element.parameters.forEach(item => {
        this.changeParamState(element.parameters, item.platformName, flag);
      });
    });
  }

  public changeDeviceState(devices: DeviceViewItem[], id: string, state?: boolean) {
    const deviceIndex = devices.indexOf(devices.find(x => x.deviceId === id));
    if (state === undefined || state === null) {
      devices[deviceIndex].deviceState = !devices[deviceIndex].deviceState;
    } else {
      devices[deviceIndex].deviceState = state;
    }
    this.zonesBufferModel.zones.forEach(zone => {
      zone.devices.forEach(item => {
        if (item.deviceId === devices[deviceIndex].deviceId) {
          item.deviceState = devices[deviceIndex].deviceState;
        }
      });
    });
    this.calculateSelectedDevices(this.zonesBufferModel.zones);
    this.calculateCappingPoints();
  }

  public changeParamState(params: ParameterViewItem[], id: string, state: boolean | null) {
    const index = params.indexOf(params.find(x => x.platformName === id));
    if (state === null || state === undefined) {
      params[index].paramState = !params[index].paramState;
    } else {
      params[index].paramState = state;
    }
    this.paramsBufferModel.paramsList.forEach(element => {
      element.parameters.forEach(item => {
        if (item.platformName === params[index].platformName) {
          item.paramState = params[index].paramState;
        }
      });
    });
    this.calculateSelectedParamsLettervise(this.paramsBufferModel.paramsList);
    // console.log('ParamsStateCHnages-----', this.paramsBufferModel);
    this.calculateCappingPoints();
  }

  public searchDevices(inputValue: string) {
    let newZonesModel = new ZonesViewModel();
    this.zonesViewModel = new ZonesViewModel();
    if (inputValue === '') {
      this.zonesViewModel = this.zonesBufferModel;
      return;
    }
    newZonesModel = JSON.parse(JSON.stringify(this.zonesBufferModel));
    newZonesModel.zones.forEach(zoneItem => {
      const filteredDevices = zoneItem.devices.filter(x => x.displayName
          .toLowerCase()
          .includes(inputValue
              .toLowerCase()));
      if (filteredDevices.length !== 0) {
        const zoneViewItem: ZoneViewItem = {
          zone: zoneItem.zone,
          devices: filteredDevices
        };
        this.zonesViewModel.zones.push(zoneViewItem);
      }
    });
  }

  public searchParams(inputValue: string) {
    let newParamsModel = new ParametersViewModel();
    this.paramsModel = new ParametersViewModel();
    if (inputValue === '') {
      this.paramsModel = this.paramsBufferModel;
      return;
    }
    newParamsModel = JSON.parse(JSON.stringify(this.paramsBufferModel));
    newParamsModel.paramsList.forEach((element, i) => {
      const filteredParams = element.parameters.filter(x => x.displayName
          .toLowerCase()
          .includes(inputValue
              .toLowerCase()));
      if (filteredParams.length !== 0) {
        const paramsItem: SelectorViewItem = {
          paramsSelector: element.paramsSelector,
          parameters: filteredParams
        };
        this.paramsModel.paramsList.push(paramsItem);
      }
    });
  }

  public downloadButton() {
    this.applyDevices();
    this.applyParams();
    this.reportDownLoadModel.reportName = this.repName;
    this.reportDownLoadModel._id.username = atob(localStorage.getItem('reportingUser'));
    this.reportDownLoadModel.singleFile = this.reportForm.get('isSingleFile').value === true ? false : true;
    this.reportDownLoadModel.email = this.emailsToDisplay;
    this.reportDownLoadModel.dataCapping = this.dataPointCount;
    this.reportDownLoadModel.configuredDate = moment().format('YYYY-MM-DDThh:mm:ss');
    localStorage.setItem('site', this.reportDownLoadModel._id.site);
    if (this.reportDownLoadModel.dateRange.startDate === '' && this.reportDownLoadModel.dateRange.endDate === '') {
      this.reportDownLoadModel.dateRange.startDate = moment(this.range.start).startOf('day').utc().milliseconds(0).toISOString();
      this.reportDownLoadModel.dateRange.endDate = moment(this.range.end).endOf('day').utc().milliseconds(0).toISOString();
    }
    this.reportDownLoadModel._id.reportId = this.reportDownLoadModel._id.site + "_" + this.reportDownLoadModel.deviceType + "_" + this.reportDownLoadModel.reportType + "_" + moment().milliseconds(0).format('YYYY-MM-DD_hh_mm_ss_SSS');
    this.reportDownLoadModel.collection = this.updateCollectionName(this.reportDownLoadModel.reportType);
    console.log('Report Final Object----', this.reportDownLoadModel);
    console.log('ValidateModel----', this.validateModel);
    this.http.saveReportConfig(this.reportDownLoadModel).subscribe(
        (reportStatus: { status: '' }) => {
          this.toastr.success(reportStatus.status + 'successfully!', 'Report Status');
          setTimeout(() => {
            this.router.navigate(['/report-details']);
          }, 100);
        },

        (error) => {
          this.toastr.error('failed to save \n Please try after some time', 'Report status');
        }
    );
  }

  // public reset() {
  //   this.model = new ReportModel();
  //   this.validateModel = new ValidateModel();
  //   this.reportDownLoadModel = new ReportDownloadModel();
  //   this.range = {start: moment(), end: moment()};
  //   this.displayDate = moment(this.range.start).format('DD/MM/YYYY') + ' - ' + moment(this.range.end).format('DD/MM/YYYY');
  //   this.minDate = null;
  //   this.tempValues = [];
  //   this.zonesViewModel = new ZonesViewModel();
  //   this.zonesBufferModel = new ZonesViewModel();
  //   this.paramsModel = new ParametersViewModel();
  //   this.paramsBufferModel = new ParametersViewModel();
  //   this.openDeviceBoxModal = false;
  //   this.openParamsBoxModal = false;
  //   this.temporatyEmail = '';
  //   this.shownDevicesList = [];
  //   this.shownParamsList = [];
  //   this.selectedParamsCount = 0;
  //   this.selectedDeviceCount = 0;
  //   this.deviceCount = 0;
  //   this.parametersCount = 0;
  //   this.dataPointCount = 0;
  //   this.emailsToDisplay = [];
  // }

  formReset() {
    this.isDeviceTypeSelected = false;
    this.reportForm.reset();
    this.reportForm.patchValue({
      isSingleFile: new FormControl(true)
    });

    // this.reset();
  }

  formResetFromResolution() {
    this.isDeviceTypeSelected = false;
    this.range = {start: moment(), end: moment()};
    this.displayDate = moment(this.range.start).format('DD/MM/YYYY') + ' - ' + moment(this.range.end).format('DD/MM/YYYY');
    this.reportForm.patchValue({
      resolution: null,
      format: null,
      deviceType: null
    });
  }

  formResetFromFormat() {
    this.isDeviceTypeSelected = false;
    this.validateModel.isReportFormatSelected = false;
    this.range = {start: moment(), end: moment()};
    this.displayDate = moment(this.range.start).format('DD/MM/YYYY') + ' - ' + moment(this.range.end).format('DD/MM/YYYY');
    this.reportForm.patchValue({
      format: null,
      deviceType: null
    });

  }

  public resetDeviceSelectionBlock() {
    this.model.deviceType = null;
    this.model.deviceModels = [];
    this.model.selectedParameters = new Array<GetParameterViewItem>();
    this.validateModel.isDevicesSelected = false;
    this.zonesViewModel = new ZonesViewModel();
    this.zonesBufferModel = new ZonesViewModel();
    this.paramsModel = new ParametersViewModel();
    this.paramsBufferModel = new ParametersViewModel();
    this.shownDevicesList = [];
    this.shownParamsList = [];
  }

  disableControls() {

  }

  calculateSelectedDevices(deviceArray: any[]) {
    this.selectedDeviceCount = 0;
    this.deviceCount = 0;
    if (deviceArray.length > 0) {
      deviceArray.forEach((zoneInfo: GetZoneViewItem) => {
        if (zoneInfo.devices.length > 0) {
          zoneInfo.devices.forEach((deviceInfo) => {
            this.deviceCount = this.deviceCount + 1;
            if (deviceInfo.deviceState) {
              this.selectedDeviceCount = this.selectedDeviceCount + 1;
            }
          });
        } else {
          this.validateModel.isDevicesSelected = false;
        }
      });

      if (this.selectedDeviceCount > 0) {
        this.validateModel.isDevicesSelected = true;
      } else {
        this.validateModel.isDevicesSelected = false;
      }
    } else {
      this.validateModel.isDevicesSelected = false;
    }
  }

  calculateSelectedParams(paramsArray: any[]) {
    this.selectedParamsCount = 0;
    this.parametersCount = 0;
    if (paramsArray.length > 0) {
      this.parametersCount = paramsArray.length;
      paramsArray.forEach((paramInfo) => {
        if (paramInfo.paramState) {
          this.selectedParamsCount = this.selectedParamsCount + 1;
        }
      });
    } else {
      this.validateModel.isParametersSelected = false;
    }

    if (this.selectedParamsCount > 0) {
      this.validateModel.isParametersSelected = true;
    } else {
      this.validateModel.isParametersSelected = false;
    }
  }

  calculateSelectedParamsLettervise(paramList: any[]) {
    this.selectedParamsCount = 0;
    paramList.forEach((data) => {
      data.parameters.forEach((paramInfo) => {
        if (paramInfo.paramState) {
          this.selectedParamsCount = this.selectedParamsCount + 1;
        }
      });
    });

    if (this.selectedParamsCount > 0) {
      this.validateModel.isParametersSelected = true;
    } else {
      this.validateModel.isParametersSelected = false;
    }
  }

  calculateTotalDays(dateRange) {
    const firstDate = dateRange.start;
    const secondDate = dateRange.end;
    const daysCount = secondDate.diff(firstDate, 'days');
    this.totolDays = daysCount === 0 ? 1 : daysCount;
    if (daysCount < 365 && firstDate && secondDate) {
      this.validateModel.isDateRangeValid = true;
    } else {
      this.validateModel.isDateRangeValid = false;
    }
  }

  calculateCappingPoints() {
    this.dataPointCount = Math.ceil(this.selectedDeviceCount * this.selectedParamsCount * this.totolDays * (DEVIDE_COUNT[this.reportDownLoadModel.reportType] / this.messageInterval));
    if (this.dataPointCount > this.totalCapping) {
      this.validateModel.isCappingValid = false;
    } else {
      this.validateModel.isCappingValid = true;
    }

    console.log("1----", this.selectedDeviceCount);
    console.log("2----", this.selectedParamsCount);
    console.log("3----", this.totolDays);
    console.log("4----", DEVIDE_COUNT[this.reportDownLoadModel.reportType]);
    console.log("5----", this.messageInterval);


  }

  onEmailAdd() {
    if (this.reportForm.get('emails').value && this.emailsToDisplay.length < 5) {
      this.emailsToDisplay.push(this.reportForm.get('emails').value);
      this.reportForm.patchValue({
        emails: null
      })
    } else {
      this.isEmailLimitExceeds = true;
      this.reportForm.patchValue({
        emails: null
      })
    }
  }

  onDeleteEmail(i: number) {
    this.emailsToDisplay.splice(i, 1);
    this.isEmailLimitExceeds = false;
  }

  displayParams() {
    if (this.shownParamsList.length > 0) {
      this.selectedParamsModal.show();
    }
  }

  displayDevices() {
    if (this.shownDevicesList.length > 0) {
      this.selectedDeviceModal.show();
    }
  }

  // function returns collection name based on resolution
  updateCollectionName(reportType) {
    if (reportType === 'Daily') {
      return "reports_daily"
    } else if (reportType === 'Hourly') {
      return "reports_timely"
    } else if (reportType === '15 Minute') {
      return "reports_timely"
    } else {
      return "customTrendResults";
    }
  }

  // function set maxDate based on resolution
  setMaxDate(resolution) {
    if (resolution === 'Generation_Hour' || resolution === 'Non_Generation_Hour' || resolution === 'Daily') {
      this.range = {start: moment().subtract(1, 'days'), end: moment().subtract(1, 'days')};
      this.displayDate = moment(this.range.start).format('DD/MM/YYYY') + ' - ' + moment(this.range.end).format('DD/MM/YYYY');
      this.maxDate = moment().subtract(1, 'days');
    } else {
      this.range = {start: moment(), end: moment()};
      this.displayDate = moment(this.range.start).format('DD/MM/YYYY') + ' - ' + moment(this.range.end).format('DD/MM/YYYY');
      this.maxDate = moment();
    }
  }

  // this function check whether devices is available for specific resolution
  checkForDevices(data, resolution) {
    if (data.length > 0) {
      // do nothing
    } else {
      this.toastr.info(`No devices available for ${resolution} resolution`)
    }
  }

}
