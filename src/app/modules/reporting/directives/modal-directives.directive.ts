import {Directive, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appModalDirectives]'
})
export class ModalDirectivesDirective {
  @Input() data;
  @Input() directiveRef;
  @Input() modalToOpen;
  @Input() modalTitle;
  constructor() {
  }

  @HostListener('mouseenter') onMouseEnter() {
    console.log('Hi Sachin');
    this.directiveRef.show();
  }
}
