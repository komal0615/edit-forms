
import { DeviceViewItem } from '../Models/zones-view-model';
import { GetParameterViewItem } from '../Models/get-params-view-model';
import {Format} from '../enums/format';

export class ReportModel {
  site: string;
  deviceType: string;
  typeAliasList: string[] = [];
  selectedParameters: GetParameterViewItem[] = [];
  deviceModels: DeviceViewItem[] = [];
  resolutionType: string;
  emails: string[] = [];
  devices: string[] = [];
  format: string;
  dateFromStr: string;
  dateToStr: string;
}
