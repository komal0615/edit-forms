import * as moment from 'moment';
import {Moment} from "moment";

export class ValidateModel {
  public isReportNameEntered = false;
  public isSiteSelected = false;
  public isResolutionSelected = false;
  public isReportFormatSelected = false;
  public isDateRangeValid = true;
  public isDeviceTypeSelected = false;
  public isDevicesSelected = true;
  public isParametersSelected = true;
  public isParamsExist = true;
  public isCappingValid = true;
  get isValid(): boolean {
    if (
      this.isReportNameEntered &&
      this.isSiteSelected &&
      this.isResolutionSelected &&
      this.isReportFormatSelected &&
      this.isDateRangeValid &&
      this.isDeviceTypeSelected &&
      this.isDevicesSelected &&
      this.isParametersSelected &&
      this.isCappingValid
    ) {
      return true;
    }
    return false;
  }

}

export class ReportDownloadModel {
  public _id: IdModel = {
    reportId : '',
    site: '',
    username: ''
  };
  deviceType: string;
  collection = 'customTrendResults';
  dateRange = {
    startDate: '',
    endDate: ''
  };
  reportName = '';
  status = 'Waiting';
  progress = 0;
  format = '';
  reportType = '';
  singleFile = false;
  deviceIds = [];
  parameters = [];
  email = [];
  dataCapping = 0;
  configuredDate: string;
  downloadedPath = '';
  hrefDownloadPath: any;
}

class IdModel {
  reportId: string;
  site: string;
  username: string;
}
