import { DeviceTypes } from '../enums/deviceTypes';
import { Format } from '../enums/format';
import { ResolutionTypes } from '../enums/resolutionTypes';

export class DropDownsModel {
  public deviceTypes = [];
  public reportTypes = [];
  public resolutionTypes = [];

  constructor() {
    this.deviceTypes = DeviceTypes.values().sort();
    this.reportTypes = Format.values().sort();
    this.resolutionTypes = ResolutionTypes.values().sort();
  }
}
