export class NotificationModel {
    public readonly incorrect = 'Incorrect e-mail address';
    public readonly repeate = 'Duplicate e-mail address';
    public readonly empty = 'Empty input field';

    public errorMessage: string;
}
