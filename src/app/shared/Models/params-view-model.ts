export class ParametersViewModel {
    paramsList: SelectorViewItem[] = [];
}
export class SelectorViewItem {
    paramsSelector: string;
    parameters: ParameterViewItem[] = [];
}
export class ParameterViewItem{
    id: string ='';
    displayName: string ='';
    platformName: string ='';
    paramState: boolean = true;
}