export class GetTypesViewModel {
    public types: GetTypeViewItem[] = [];
    public typesDictionary: any = {};
}
export class GetTypeViewItem {
    public type: string = '';
    public zones: GetZoneViewItem[] = [];
}
export class GetZoneViewItem {
    public zone: string= '';
    public devices: GetDeviceViewItem[] = [];
}
export class GetDeviceViewItem {
    public id: string = '';
    public deviceState: boolean;
    public deviceId: string = '';
    public displayName: string ='';
}
