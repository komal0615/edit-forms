export class configParametersViewModel {
  resolutionDeviceTypes: TypeViewItem[] = [];
  resolutionDeviceTypesList: string[] = [];
}

export class GetParametersViewModel {
    public minuteDeviceTypes: TypeViewItem[] = [];
    public minuteDeviceTypesList: string[] = [];
    public dailyDeviceTypes: TypeViewItem[] = [];
    public dailyDeviceTypesList: string[] = [];
    public hourlyDeviceTypes:TypeViewItem[] = [];
    public hourlyDeviceTypesList: string[] = [];
    public fifteenMinuteDeviceTypes: TypeViewItem[] = [];
    public fifteenMinuteDeviceTypesList: string[] = [];
    public genHoursDeviceTypes: TypeViewItem[] = [];
    public genHoursDeviceTypesList: string[] = [];
    public nonGenHoursDeviceTypes: TypeViewItem[] = [];
    public nonGenHoursDeviceTypesList: string[] = [];
}
export class TypeViewItem {
    type: string = '';
    parameters: GetParameterViewItem[] = [];
}
export class GetParameterViewItem {
    id: string = '';
    displayName: string = '';
    platformName: string = '';
    paramState: boolean = true;
}
