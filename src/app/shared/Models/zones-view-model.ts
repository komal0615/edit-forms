export class ZonesViewModel {
  public zones: ZoneViewItem[] = [];
}

export class ZoneViewItem {
  public zone: string = '';
  public devices: DeviceViewItem[] = [];
}

export class DeviceViewItem {
  public id: string = '';
  public deviceState: boolean;
  public deviceId: string = '';
  public displayName: string = '';
}
