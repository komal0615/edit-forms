export class GetSitesViewModel {
    public sites: SiteGetSitesViewItem[];
    constructor() {
        this.sites = [];
    }
}
export class SiteGetSitesViewItem {
    site: string;
    customer: string;
    dateOfCommissioning: string;
}
export class IdDetails {
    site: string;
    customer: string;
}
