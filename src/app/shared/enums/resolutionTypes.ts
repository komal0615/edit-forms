type ResolutionTypes =
  'Daily' |
  'Minute'|
  'Hourly'|
  '15 Minute'|
  'Generation_Hour'|
  'Non_Generation_Hour';
module ResolutionTypes {
  export const Daily: ResolutionTypes = 'Daily';
  export const Minute: ResolutionTypes = 'Minute';
  export const Hourly: ResolutionTypes = 'Hourly';
  export const Minute15: ResolutionTypes = '15 Minute';
  export const GenerationHour : ResolutionTypes = 'Generation_Hour';
  export const NonGenerationHour: ResolutionTypes = 'Non_Generation_Hour';
  export function values(): ResolutionTypes[] {
    return [
        Daily,
        Minute
    ];
  }
};

export { ResolutionTypes }
