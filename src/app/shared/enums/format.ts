type Format =
  'xlsx' |
  'csv';
module Format {
  export const Xlsx: Format = 'xlsx';
  export const Csv: Format = 'csv';
  export function values(): Format[] {
    return [
      Csv,
      Xlsx
    ];
  }
};

export { Format }
