enum ErrorCode {
    correct = 0,
    empty = 1,
    incorrect = 2,
    repeate = 3
}
export { ErrorCode };
