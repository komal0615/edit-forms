type DeviceTypes =
  'INV' |
  'INVERTER' |
  'WSTATION' |
  'TRACKER' |
  'GMETER' |
  'TRAFO'
module DeviceTypes {
  export const INV: DeviceTypes = 'INV';
  export const INVERTER: DeviceTypes = 'INVERTER';
  export const WSTATION: DeviceTypes = 'WSTATION';
  export const TRACKER: DeviceTypes = 'TRACKER';
  export const GMETER: DeviceTypes = 'GMETER';
  export const TRAFO: DeviceTypes = 'TRAFO';
  export function values(): DeviceTypes[] {
    return [
      INV,
      INVERTER,
      WSTATION,
      TRACKER,
      GMETER,
      TRAFO
    ];
  }
}

export { DeviceTypes };
