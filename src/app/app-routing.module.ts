import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReportingModule} from './modules/reporting/reporting.module';
import {ReportDetailsComponent} from './modules/reporting/components/report-details/report-details.component';
import {NewReportComponent} from './modules/reporting/components/new-report/new-report.component';
import {ErrorComponent} from './error/error.component';
import {AuthGuard} from './core/guards/auth-guard';
import {EditFormsComponent} from './modules/reporting/components/edit-forms/edit-forms.component';

const routes: Routes = [
  {path: '', component: ErrorComponent, pathMatch: 'full'},
  {path: 'report-details', component: ReportDetailsComponent, canActivate: [AuthGuard]},
  {path: 'new-report', component: NewReportComponent, canActivate: [AuthGuard]},
  {path: 'edit-form', component: EditFormsComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [
    ReportingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
