import {Component} from '@angular/core';
import {ConnectionService} from "ng-connection-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'solar-reporting-app';
  loggedInUser = '';
  currentDate = new Date();
  status = 'ONLINE';
  isConnected = true;
  constructor(private connectionService: ConnectionService) {
    this.checkInternetConnection();
    setTimeout(() => {
      if (localStorage.getItem('reportingUser')) {
        this.loggedInUser = atob(localStorage.getItem('reportingUser'));
      } else {
        this.loggedInUser = '';
      }
    },1500)

  }

  checkInternetConnection() {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.status = "ONLINE";
      }
      else {
        this.status = "OFFLINE";
      }
    })
  }
}
